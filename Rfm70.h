#ifndef RFM70CPP_RFM70_H__
#define RFM70CPP_RFM70_H__

#include <cstdint>

#include "rfm70_reg.h"

namespace rfm70{
	enum class Mode{
		PowerDown,
		Standby,
		Rx,
		Tx
	};

	enum class PipeID : uint8_t {
		pipe0 = 0,
		pipe1 = 1,
		pipe2 = 2,
		pipe3 = 3,
		pipe4 = 4,
		pipe5 = 5,
	};

	enum class LnaGain : uint8_t{
		Low = 0,
		High = 1
	};

	enum class RfPower : uint8_t{
		Neg10dBm = 0,
		Neg5dBm = 1,
		Pos0dBm = 2,
		Pos5dBm = 3
	};

	enum class CrcMode : uint8_t {
		crc_off = 0,
		crc_8bit = 1,
		crc_16bit = 2
	};

	enum class AirDataRate : uint8_t{
		Mbps1 = 0,
		Mbps2 = 1
	};

	enum class AddressWidth : uint8_t{
		AW3 = 1,
		AW4 = 2,
		AW5 = 3
	};

	enum class AutoRetransmissionDelay : uint8_t{
		Wait250us = 0,
		Wait500us = 1,
		Wait750us = 2,
		Wait1000us = 3,
		Wait1250us = 4,
		Wait1500us = 5,
		Wait1750us = 6,
		Wait2000us = 7,
		Wait2250us = 8,
		Wait2500us = 9,
		Wait2750us = 10,
		Wait3000us = 11,
		Wait3250us = 12,
		Wait3500us = 13,
		Wait3750us = 14,
		Wait4000us = 15
	};

	struct AutoRetransmissionCount {
		static constexpr uint8_t min = 0;
		static constexpr uint8_t max = 15;
	};

	template<typename Base>
	class transceiver : public Base{
	public:
		transceiver()
			: activeMode(Mode::PowerDown), _bank_id(0)
		{
		}
		void init(){
			Base::delay_ms(200);
			_bank_id = reg_read<Reg::STATUS>().RBANK;
			setMode(Mode::Standby);
			initBank0();
			initBank1();
			flush_rx();
			flush_tx();
		}

        unsigned char getReceivePipe(){
           auto status = reg_read<Reg::STATUS>();
           return status.RX_P_NO;
        }

		bool write_tx_payload(unsigned char* buffer, unsigned char size, bool ack){
			if(reg_read<Reg::FIFO_STATUS>().TX_FULL){
				return false;
			}
			if(ack){
				doCmdBufferWrite<Cmd::W_TX_PAYLOAD>(buffer, size);
			}else{
				doCmdBufferWrite<Cmd::W_TX_PAYLOAD_NOACK>(buffer, size);
			}
			return true;
		}


		void set_crc_mode(CrcMode mode)
		{
			auto config = reg_read<Reg::CONFIG>();

			switch (mode)
			{
				case CrcMode::crc_off:
					config.EN_CRC = 0;
					break;
				case CrcMode::crc_8bit:
					config.EN_CRC = 1;
					config.CRCO = 0;
					break;
				case CrcMode::crc_16bit:
					config.EN_CRC = 1;
					config.CRCO = 1;
					break;
				default:
					break;
			}

			regWrite(config);
		}


		/*void set_pipe_dynamic_payload_enabled(PipeID pipe, bool enabled){
			auto reg = reg_read<Reg::DYNPD>();
			reg.value = (reg.value & ~(1 << (uint8_t)pipe)) | ((uint8_t)enabled << (uint8_t)pipe);
			reg_write(reg);
		}*/

		void set_pipe_payload_length(PipeID pipe, uint8_t length){
			auto dynpd = reg_read<Reg::DYNPD>();
			if(length == 0){
				dynpd.value |= 1 << (uint8_t)pipe;
			}else{
				dynpd.value &= ~(1 << (uint8_t)pipe);
			}
			reg_write(dynpd);

			switch(pipe){
			case PipeID::pipe0:
				reg_write(Reg::RX_PW_P0(length));
				break;
			case PipeID::pipe1:
				reg_write(Reg::RX_PW_P1(length));
				break;
			case PipeID::pipe2:
				reg_write(Reg::RX_PW_P2(length));
				break;
			case PipeID::pipe3:
				reg_write(Reg::RX_PW_P3(length));
				break;
			case PipeID::pipe4:
				reg_write(Reg::RX_PW_P4(length));
				break;
			case PipeID::pipe5:
				reg_write(Reg::RX_PW_P5(length));
				break;
			}
		}

		void reuse_tx_pl(){
			doCmd<rfm70::Cmd::REUSE_TX_PL>();
		}

		uint8_t read_rx_payload(uint8_t* buffer){
			auto status = reg_read<Reg::STATUS>();
			if (status.RX_P_NO != 0x7){
				uint8_t length = getReceivedLength();
				if (length <= 32){
					doCmdBufferRead<Cmd::R_RX_PAYLOAD>(buffer, length);
				}else{
					flush_rx();
					length = 0;
				}
				return length;
			}
			return 0;
		}
		void setRxAddressPipe0(const uint8_t address[5]){
			reg_write(Reg::RX_ADDR_P0(address));
		}
		void setRxAddressPipe1(const uint8_t address[5]){
			reg_write(Reg::RX_ADDR_P1(address));
		}
		void setTxAddress(const uint8_t address[5]){
			reg_write(Reg::TX_ADDR(address));
		}
		void setChannel(unsigned char channel){
			reg_write<Reg::RF_CH>(channel & 0x7f);
		}
		void flush_rx(){
			doCmd<Cmd::FLUSH_RX>();
		}
		void flush_tx(){
			doCmd<Cmd::FLUSH_TX>();
		}
		unsigned char getReceivedLength(){
			return doCmd<Cmd::R_RX_PL_WID>();
		}
		void setMode(Mode mode){
			if(mode == activeMode){
				return;
			}
			switch(mode){
			case Mode::PowerDown:{
				Base::ce(GPIO_PIN_RESET);
					auto cfg = reg_read<Reg::CONFIG>();
					cfg.PWR_UP = 0;
					reg_write(cfg);
				}
				break;
			case Mode::Standby:{
				Base::ce(GPIO_PIN_RESET);
					auto cfg = reg_read<Reg::CONFIG>();
					cfg.PWR_UP = 1;//TODO: maybe clear flags PRIM_RX PRIM_TX
					reg_write(cfg);
				}
				break;
			case Mode::Rx:{
					flush_rx();
					cli();
					Base::ce(GPIO_PIN_RESET);
					auto cfg = reg_read<Reg::CONFIG>();
					cfg.PRIM_RX = 1;
					cfg.PWR_UP = 1;
					reg_write(cfg);
					Base::ce(GPIO_PIN_SET);
				}
				break;
			case Mode::Tx:{
					flush_tx();
					cli();
					Base::ce(GPIO_PIN_RESET);
					auto cfg = reg_read<Reg::CONFIG>();
					cfg.PRIM_RX = 0;
					cfg.PWR_UP = 1;
					reg_write(cfg);
					Base::ce(GPIO_PIN_SET);
				}
				break;
			}
			activeMode = mode;
		}

		void set_pipe_aa_enabled(PipeID pipe, bool enabled){
			auto reg_enaa = reg_read<Reg::EN_AA>();
			reg_enaa.value = (reg_enaa.value & ~(1 << (uint8_t)pipe)) | ((int)enabled << (uint8_t)pipe);
			reg_write(reg_enaa);
		}

		void set_rx_pipe_enabled(PipeID pipe, bool enabled){
			auto reg = reg_read<Reg::EN_RXADDR>();
			reg.value = (reg.value & ~(1 << (uint8_t)pipe)) | ((uint8_t)enabled << (uint8_t)pipe);
			reg_write(reg);
		}
		bool get_rx_pipe_open(PipeID pipe){
			return (reg_read<Reg::EN_RXADDR>().value & (1 << (uint8_t)pipe)) != 0;
		}
		// clear interrupt status
		Reg::STATUS cli(){
			auto status = reg_read<Reg::STATUS>();
			reg_write(status);
			return status;
		}
		void setLnaGain(LnaGain gain){
			auto setup = reg_read<Reg::RF_SETUP>();
			setup.LNA_HCURR = gain;
			regWrite(setup);
		}
		LnaGain getLnaGain(){
			return (LnaGain)reg_read<Reg::RF_SETUP>().LNA_HCURR;
		}
		void setRfPwr(RfPower pwr){
			auto setup = reg_read<Reg::RF_SETUP>();
			setup.RF_PWR = pwr;
			regWrite(setup);
		}
		RfPower getRfPwr(){
			return (RfPower)reg_read<Reg::RF_SETUP>().RF_PWR;
		}
		void setAirDataRate(AirDataRate rate){
			auto setup = reg_read<Reg::RF_SETUP>();
			setup.RF_DR = (uint8_t)rate;
			reg_write(setup);
		}
		AirDataRate getAirDataRate(){
			return (AirDataRate)reg_read<Reg::RF_SETUP>().RF_DR;
		}

        void switchBank(unsigned char b){
            if(_bank_id != b){
                doCmd<Cmd::ACTIVATE>(0x53);
                _bank_id = b;
            }
        }
        template<typename _Register>
        bool isRegisterWritable(){
            auto tmpValue = reg_read<_Register>().value;
            auto testValue = tmpValue == 0 ? 1 : 0;
            reg_write(_Register(testValue));
            auto result = reg_read<_Register>().value;
            if(tmpValue != result){
            	reg_write(_Register(tmpValue));
            }
            return tmpValue != result;
        }

        template<typename _Cmd>
        unsigned char doCmd(){
        	return Base::readByte(_Cmd::value, 0);
        }
        template<typename _Cmd>
        unsigned char doCmd(unsigned char val){
        	return Base::readByte(_Cmd::value, val);
        }

        void doCmdBufferWrite(uint8_t command, const uint8_t* val, unsigned char size){
			Base::writeBuffer(command, val, size);
		  }

        template<typename _Cmd>
        void doCmdBufferWrite(unsigned char* val, unsigned char size){
        	Base::writeBuffer(_Cmd::value, val, size);
        }

        void doCmdBufferRead(uint8_t command, unsigned char* val, unsigned char size){
        	Base::readBuffer(command, val, size);
        }

        template<typename _Cmd>
        void doCmdBufferRead(unsigned char* val, unsigned char size){
        	Base::readBuffer(_Cmd::value, val, size);
        }

        template<typename _Register>
        _Register reg_read(){
        	switchBank(_Register::bank);
            return RegManager<Base, _Register, typename _Register::Type>::regRead();
        }
        template<typename _Register>
        void reg_write(_Register reg){
        	switchBank(_Register::bank);
            RegManager<Base, _Register, typename _Register::Type>::regWrite(reg);
        }

        void set_address_width(AddressWidth width){
        	reg_write(Reg::SETUP_AW ((uint8_t)width));
        }

        void set_auto_retransmission(AutoRetransmissionDelay delay, uint8_t count){
        	Reg::SETUP_RETR reg;
        	reg.ARD = (uint8_t)delay;
        	reg.ARC = count;
        	reg_write(reg);
        }
        bool write_ack_payload(PipeID pipe, const uint8_t *payload, uint8_t length) {
        	auto status = reg_read<Reg::FIFO_STATUS>();
        	if(status.TX_FULL){
				return false;
			}
        	doCmdBufferWrite(Cmd::W_ACK_PAYLOAD::value | (uint8_t)pipe, payload, length);
        	return true;
        }
        void enable_ack_payload(bool enable)
        {
          auto feature = reg_read<Reg::FEATURE>();
          feature.EN_ACK_PAY = enable ? 1 : 0;
          reg_write(feature);
        }
	private:
        bool getExtraFeaturesEnabled(){
        	return isRegisterWritable<Reg::FEATURE>();
		}
		void setExtraFeaturesEnabled(bool enable){
			bool enabled = getExtraFeaturesEnabled();
			if(enable != enabled){
				doCmd<Cmd::ACTIVATE>(0x73);
			}
		}
		void initBank0(){
			//switchBank(0);
			/*auto cfg = Reg::CONFIG(0);
			cfg.PRIM_RX = 1;
			cfg.EN_CRC = 1;
			cfg.CRCO = 1;
			cfg.PWR_UP = 1;
			reg_write(cfg);
			reg_write(Reg::EN_AA (0x3F));//Enable auto acknowledgement data pipe5\4\3\2\1\0
			reg_write(Reg::EN_RXADDR (0x3));//Enable RX Addresses pipe 0, 1
			reg_write(Reg::SETUP_AW (0x03));//RX/TX address field width 5byte
			reg_write(Reg::SETUP_RETR (0xF3));//auto retransmission delay (4000us),auto retransmission count(3)
			reg_write(Reg::RF_CH (0x17));//23 channel
			reg_write(Reg::RF_SETUP (0x3F));//air data rate-1M,out power 0dbm,setup LNA gain
			reg_write(Reg::STATUS (0x07));
			reg_write(Reg::OBSERVE_TX (0x00));
			reg_write(Reg::CD (0x00));
			reg_write(Reg::RX_ADDR_P2(0xc3));//only LSB Receive address data pipe 2, MSB bytes is equal to RX_ADDR_P1[39:8]
			reg_write(Reg::RX_ADDR_P3(0xc4));//only LSB Receive address data pipe 3, MSB bytes is equal to RX_ADDR_P1[39:8]
			reg_write(Reg::RX_ADDR_P4(0xc5));//only LSB Receive address data pipe 4, MSB bytes is equal to RX_ADDR_P1[39:8]
			reg_write(Reg::RX_ADDR_P5(0xc6));//only LSB Receive address data pipe 5, MSB bytes is equal to RX_ADDR_P1[39:8]
			reg_write(Reg::RX_PW_P0(0x20));//Number of bytes in RX payload in data pipe0(32 byte)
			reg_write(Reg::RX_PW_P1(0x20));//Number of bytes in RX payload in data pipe1(32 byte)
			reg_write(Reg::RX_PW_P2(0x20));//Number of bytes in RX payload in data pipe2(32 byte)
			reg_write(Reg::RX_PW_P3(0x20));//Number of bytes in RX payload in data pipe3(32 byte)
			reg_write(Reg::RX_PW_P4(0x20));//Number of bytes in RX payload in data pipe4(32 byte)
			reg_write(Reg::RX_PW_P5(0x20));//Number of bytes in RX payload in data pipe5(32 byte)
			reg_write(Reg::FIFO_STATUS(0x00));//fifo status
			reg_write(Reg::DYNPD(0x3F));//Enable dynamic payload length data pipe5\4\3\2\1\0
*/
			//setup default RX & TX addresses
			unsigned char Address0[]={0xC0,0xC0,0xC0,0xC0,0xC0};//Receive address data pipe 0
			unsigned char Address1[]={0xC1,0xC1,0xC1,0xC1,0xC1};//Receive address data pipe 1
			setRxAddressPipe0(Address0);
			setRxAddressPipe1(Address1);
			setTxAddress(Address0);

			//enable extra features by default
			setExtraFeaturesEnabled(true);
			reg_write(Reg::FEATURE(7));

		}
		void initBank1(){
			switchBank(1);

			//reversed byte order (MSB first)
			const unsigned char r0[] = {0x40, 0x4B, 0x01, 0xE2};
			reg_write(Reg::BANK1_UNKNOWN_00(r0));

			const unsigned char r1[] = {0xC0, 0x4B, 0x00, 0x00};
			reg_write(Reg::BANK1_UNKNOWN_01(r1));

			const unsigned char r2[] = {0xD0, 0xFC, 0x8C, 0x02};
			reg_write(Reg::BANK1_UNKNOWN_02(r2));

			const unsigned char r3[] = {0x99, 0x00, 0x39, 0x41};
			reg_write(Reg::BANK1_UNKNOWN_03(r3));

			const unsigned char r4[] = {0xD9, 0x9E, 0x86, 0x0B};
			reg_write(Reg::BANK1_UNKNOWN_04(r4));

			const unsigned char r5[] = {0x24, 0x06, 0x7F, 0xA6};
			reg_write(Reg::BANK1_UNKNOWN_05(r5));

			//normal byte order (LSB first)
			const unsigned char rC[] = {0x00, 0x12, 0x73, 0x00};
			reg_write(Reg::BANK1_UNKNOWN_0C(rC));

			const unsigned char rFeature[] = {0x36, 0xB4, 0x80, 0x00};
			reg_write(Reg::NEW_FEATURE(rFeature));

			//set ramp curve
			const unsigned char bank1_init_ramp_data[11] = { 0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF }; // LSB first
			reg_write(Reg::RAMP(bank1_init_ramp_data));

			Base::delay_ms(100);
			switchBank(0);
		}



		template<typename _Man, typename _Register, typename _RegisterType>
		class RegManager{
		};

		template<typename _Man, typename _Register>
		struct RegManager<_Man, _Register, RegMode::Single>{
			static _Register regRead(){
				return _Register(_Man::readByte(_Register::address | rfm70::Cmd::R_REGISTER::value, 0));
			}
			static void regWrite(_Register reg){
				_Man::readByte(_Register::address | rfm70::Cmd::W_REGISTER::value, reg.value);
			}
		};
		template<typename _Man, typename _Register>
		struct RegManager<_Man, _Register, RegMode::Buffer>{
			static _Register regRead(){
				_Register reg;
				_Man::readBuffer(_Register::address | rfm70::Cmd::R_REGISTER::value, reg.value, _Register::size);
				return reg;
			}
			static void regWrite(_Register reg){
				_Man::writeBuffer(_Register::address | rfm70::Cmd::W_REGISTER::value, reg.value, _Register::size);
			}
		};

		Mode activeMode;
		uint32_t _bank_id;
	};
}
#endif  //RFM70CPP_
