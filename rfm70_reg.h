#ifndef RFM70_REG_H__
#define RFM70_REG_H__

#include <cstdint>

namespace rfm70{
	//config
	#define USE_REGISTER_NAMES
	#define RFM70_REGISTER_BASIC_TYPE unsigned char

	struct RegMode{
		struct Single{};
		struct Buffer{};
	};

	#ifndef RFM70_REGISTER_BASIC_TYPE
		#error "RFM70_REGISTER_BASIC_TYPE should be defined first! Typically it is u8 type"
	#endif


	#define CONCAT(...) __VA_ARGS__

	//helper macro
	#ifdef USE_REGISTER_NAMES
	#define REGISTER_NAME_FUNC(__REGISTER_NAME)\
		static inline const char* Name() { return #__REGISTER_NAME; }
	#else
	#define REGISTER_NAME_FUNC(__REGISTER_NAME)
	#endif

	#define REGISTER_ADDRESS_FUNC(__REGISTER_ADDRESS)\
		static constexpr uint8_t address = __REGISTER_ADDRESS;

	#define REGISTER_BANK_FUNC(__REGISTER_BANK)\
		static constexpr uint8_t bank = __REGISTER_BANK;

	#define REGISTER_RESET_VAL_FUNC(__REG, __RESET_VAL)\
		static inline __REG reset_value() { return __REG(__RESET_VAL); }

	#define BUFFER_REGISTER_RESET_VAL_FUNC(__REG, __SIZE, __RESET_VAL)\
		static inline __REG reset_value() { \
			static const unsigned char val[__SIZE] = {__RESET_VAL};\
			return __REG(val); \
		}

	//register
	#define REGISTER(__REGISTER_ADDRESS, __REGISTER_BANK, __REGISTER_NAME, __RESET_VAL, ...)\
	struct __REGISTER_NAME {\
		__REGISTER_NAME(RFM70_REGISTER_BASIC_TYPE val) { value = val; }\
		__REGISTER_NAME() { }\
		REGISTER_NAME_FUNC(__REGISTER_NAME)\
		REGISTER_ADDRESS_FUNC(__REGISTER_ADDRESS)\
		REGISTER_BANK_FUNC(__REGISTER_BANK)\
		REGISTER_RESET_VAL_FUNC(__REGISTER_NAME, __RESET_VAL)\
		typedef RegMode::Single Type;\
		union {\
			RFM70_REGISTER_BASIC_TYPE value; \
			struct {__VA_ARGS__}; \
		}; \
	};

	#define BUFFER_REGISTER(__REGISTER_ADDRESS, __REGISTER_BANK, __REGISTER_NAME, __SIZE, __RESET_VAL)\
	struct __REGISTER_NAME {\
		__REGISTER_NAME() { }\
		__REGISTER_NAME(const unsigned char val[]){\
			for(unsigned char i = 0; i < __SIZE; ++i){value[i] = val[i];}\
		}\
		REGISTER_NAME_FUNC(__REGISTER_NAME)\
		REGISTER_ADDRESS_FUNC(__REGISTER_ADDRESS)\
		REGISTER_BANK_FUNC(__REGISTER_BANK)\
		BUFFER_REGISTER_RESET_VAL_FUNC(__REGISTER_NAME, __SIZE, CONCAT(__RESET_VAL))\
		typedef RegMode::Buffer Type;\
		unsigned char value[__SIZE];\
		static constexpr uint8_t size = __SIZE;\
	};

	#define TZ ;
	#define DT :
	#define REG_BM(__NAME, __BITS_CNT) RFM70_REGISTER_BASIC_TYPE __NAME DT __BITS_CNT TZ
	#define REG_B1(__NAME) RFM70_REGISTER_BASIC_TYPE __NAME DT 1 TZ

	struct Reg {
		//Bank 0 registers

		//Configuration Register
		REGISTER(0x00, 0, CONFIG, 0x8,
			REG_B1(PRIM_RX)		//RX/TX control, 1: PRX, 0: PTX
			REG_B1(PWR_UP)		//1: POWER UP, 0:POWER DOWN
			REG_B1(CRCO)		//CRC encoding scheme '0' - 1 byte '1' - 2 bytes
			REG_B1(EN_CRC)		//Enable CRC. Forced high if one of the bits in the EN_AA is high
			REG_B1(MASK_MAX_RT)	//Mask interrupt caused by MAX_RT 1: Interrupt not reflected on the IRQ pin 0: Reflect MAX_RT as active low interrupt on the IRQ pin
			REG_B1(MASK_TX_DS)	//Mask interrupt caused by TX_DS 1: Interrupt not reflected on the IRQ pin 0: Reflect TX_DS as active low interrupt on the IRQ pin
			REG_B1(MASK_RX_DR)	//Mask interrupt caused by RX_DR 1: Interrupt not reflected on the IRQ pin 0: Reflect RX_DR as active low interrupt on the IRQ pin
			REG_B1(Reserved) 	//Only '0' allowed
		);
		//Enable "Auto Acknowledgment" Function
		REGISTER(0x01, 0, EN_AA, 0x3f,
			REG_B1(ENAA_P0)		//Enable auto acknowledgement data pipe 0
			REG_B1(ENAA_P1)		//Enable auto acknowledgement data pipe 1
			REG_B1(ENAA_P2)		//Enable auto acknowledgement data pipe 2
			REG_B1(ENAA_P3)		//Enable auto acknowledgement data pipe 3
			REG_B1(ENAA_P4)		//Enable auto acknowledgement data pipe 4
			REG_B1(ENAA_P5)		//Enable auto acknowledgement data pipe 5
			REG_BM(Reserved, 2)	//Only '00' allowed
		);
		//Enabled RX Addresses
		REGISTER(0x02, 0, EN_RXADDR, 0x3,
			REG_B1(ERX_P0)		//Enable data pipe 0
			REG_B1(ERX_P1)		//Enable data pipe 1
			REG_B1(ERX_P2)		//Enable data pipe 2
			REG_B1(ERX_P3)		//Enable data pipe 3
			REG_B1(ERX_P4)		//Enable data pipe 4
			REG_B1(ERX_P5)		//Enable data pipe 5
			REG_BM(Reserved, 2)	//Only '00' allowed
		);
		//Setup of Address Widths (common for all data pipes)
		REGISTER(0x03, 0, SETUP_AW, 0x3,
			REG_BM(AW, 2)		//RX/TX Address field width '00' - Illegal '01' - 3 bytes '10' - 4 bytes '11' - 5 bytes LSB bytes are used if address width is below 5 bytes
			REG_BM(Reserved, 6)	//Only '000000' allowed
		);
		//Setup of Automatic Retransmission
		REGISTER(0x04, 0, SETUP_RETR, 0x3,
			REG_BM(ARC, 4)		//Auto Retransmission Count „0000‟ –Re-Transmit disabled „0001‟ – Up to 1 Re-Transmission on fail of AA …… „1111‟ – Up to 15 Re-Transmission on fail of AA
			REG_BM(ARD, 4)		//Auto Retransmission Delay „0000‟ – Wait 250 us „0001‟ – Wait 500 us „0010‟ – Wait 750 us …….. „1111‟ – Wait 4000 us (Delay defined from end of transmission to start of next transmission)
		);
		//RF Channel
		REGISTER(0x05, 0, RF_CH, 0x2,
			REG_BM(RF_CH_, 7)	//Sets the frequency channel
			REG_B1(Reserved)	//Only '0' allowed
		);
		//RF Setup Register
		REGISTER(0x06, 0, RF_SETUP, 0x3f,
			REG_B1(LNA_HCURR)	//Setup LNA gain 0:Low gain(20dB down) 1:High gain
			REG_BM(RF_PWR, 2)	//Set RF output power in TX mode RF_PWR[1:0] '00' – -10 dBm '01' – -5 dBm '10' – 0 dBm '11' – 5 dBm
			REG_B1(RF_DR)		//Air Data Rate "0" – 1Mbps "1" – 2Mbps
			REG_BM(Reserved, 4)	//reserved (4-7) 1100
		);
		//Status Register (In parallel to the SPI
		//command word applied on the MOSI pin,
		//the STATUS register is shifted serially out
		//on the MISO pin)
		REGISTER(0x07, 0, STATUS, 0xe,
			REG_B1(TX_FULL)		//TX FIFO full flag. 1: TX FIFO full 0: Available locations in TX FIFO
			REG_BM(RX_P_NO, 3)	//Data pipe number for the payload available for reading from RX_FIFO 000-101: Data Pipe Number 110: Not used 111: RX FIFO Empty
			REG_B1(MAX_RT)		//Maximum number of TX retransmits interrupt Write 1 to clear bit. If MAX_RT is asserted it must be cleared to enable further communication.
			REG_B1(TX_DS)		//Data Sent TX FIFO interrupt Asserted when packet transmitted on TX. If AUTO_ACK is activated, this bit is set high only when ACK is received. Write 1 to clear bit.
			REG_B1(RX_DR)		//Data Ready RX FIFO interrupt Asserted when new data arrives RX FIFO Write 1 to clear bit
			REG_B1(RBANK)		//Register bank selection states. Switch register bank is done by SPI command "ACTIVATE" followed by 0x53 0: Register bank 0
		);
		//Transmit observe register
		REGISTER(0x08, 0, OBSERVE_TX, 0,
			REG_BM(ARC_CNT, 4)	//Count retransmitted packets. The counter is reset when transmission of a new packet starts
			REG_BM(PLOS_CNT, 4)	//Count lost packets. The counter is overflow protected to 15, and discontinues at max until reset. The counter is reset by writing to RF_CH.
		);
		//Carrier Detect
		REGISTER(0x09, 0, CD, 0,
			REG_B1(CD_)			//Carrier Detect
			REG_BM(Reserved, 7)	//Reserved, 0
		);
		BUFFER_REGISTER(0x0A, 0, RX_ADDR_P0, 5, CONCAT(0xE7,0xE7,0xE7,0xE7,0xE7));	//Receive address data pipe 0. 5 Bytes maximum length. (LSB byte is written first. Write the number of bytes defined by SETUP_AW)
		BUFFER_REGISTER(0x0B, 0, RX_ADDR_P1, 5, CONCAT(0xC2,0xC2,0xC2,0xC2,0xC2));	//Receive address data pipe 1. 5 Bytes maximum length. (LSB byte is written first. Write the number of bytes defined by SETUP_AW)
		REGISTER(0x0C, 0, RX_ADDR_P2, 0xC3, REG_BM(RX_ADDR_P2_, 8));	//Receive address data pipe 2. Only LSB MSB bytes is equal to RX_ADDR_P1[39:8]
		REGISTER(0x0D, 0, RX_ADDR_P3, 0xC4, REG_BM(RX_ADDR_P3_, 8));	//Receive address data pipe 3. Only LSB MSB bytes is equal to RX_ADDR_P1[39:8]
		REGISTER(0x0E, 0, RX_ADDR_P4, 0xC5, REG_BM(RX_ADDR_P4_, 8));	//Receive address data pipe 4. Only LSB. MSB bytes is equal to RX_ADDR_P1[39:8]
		REGISTER(0x0F, 0, RX_ADDR_P5, 0xC6, REG_BM(RX_ADDR_P5_, 8));	//Receive address data pipe 5. Only LSB. MSB bytes is equal to RX_ADDR_P1[39:8]
		BUFFER_REGISTER(0x10, 0, TX_ADDR, 5, CONCAT(0xE7,0xE7,0xE7,0xE7,0xE7));	//Transmit address. Used for a PTX device only. (LSB byte is written first) Set RX_ADDR_P0 equal to this address to handle automatic acknowledge if this is a PTX device
		REGISTER(0x11, 0, RX_PW_P0, 0,
			REG_BM(RX_PW_P0_, 6)	//Number of bytes in RX payload in data pipe0 (1 to 32 bytes). 0: not used 1 = 1 byte … 32 = 32 bytes
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		REGISTER(0x12, 0, RX_PW_P1, 0,
			REG_BM(RX_PW_P1_, 6)
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		REGISTER(0x13, 0, RX_PW_P2, 0,
			REG_BM(RX_PW_P2_, 6)
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		REGISTER(0x14, 0, RX_PW_P3, 0,
			REG_BM(RX_PW_P3_, 6)
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		REGISTER(0x15, 0, RX_PW_P4, 0,
			REG_BM(RX_PW_P4_, 6)
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		REGISTER(0x16, 0, RX_PW_P5, 0,
			REG_BM(RX_PW_P5_, 6)
			REG_BM(Reserved, 2)		//Only '00' allowed
		);
		//FIFO Status Register
		REGISTER(0x17, 0, FIFO_STATUS, 0x11,
			REG_B1(RX_EMPTY)		//RX FIFO empty flag 1: RX FIFO empty 0: Data in RX FIFO
			REG_B1(RX_FULL)			//RX FIFO full flag 1: RX FIFO full 0: Available locations in RX FIFO
			REG_BM(Reserved1, 2)	//Only '00' allowed
			REG_B1(TX_EMPTY)		//TX FIFO empty flag. 1: TX FIFO empty 0: Data in TX FIFO
			REG_B1(TX_FULL)			//TX FIFO full flag 1: TX FIFO full; 0: Available locations in TX FIFO
			REG_B1(TX_REUSE)		//Reuse last transmitted data packet if set high
			REG_B1(Reserved2)		//Only '0' allowed
		);
		//Enable dynamic payload length
		REGISTER(0x1C, 0, DYNPD, 0x0,
			REG_B1(DPL_P0)			//Enable dynamic payload length data pipe 0.(Requires EN_DPL and ENAA_P0)
			REG_B1(DPL_P1)			//Enable dynamic payload length data pipe 1.(Requires EN_DPL and ENAA_P1)
			REG_B1(DPL_P2)			//Enable dynamic payload length data pipe 2.(Requires EN_DPL and ENAA_P2)
			REG_B1(DPL_P3)			//Enable dynamic payload length data pipe 3.(Requires EN_DPL and ENAA_P3)
			REG_B1(DPL_P4)			//Enable dynamic payload length data pipe 4.(Requires EN_DPL and ENAA_P4)
			REG_B1(DPL_P5)			//Enable dynamic payload length data pipe 5.(Requires EN_DPL and ENAA_P5)
			REG_BM(Reserved, 2)		//Only "00" allowed
		);
		//Feature Register
		REGISTER(0x1D, 0, FEATURE, 0x0,
			REG_B1(EN_DYN_ACK)		//Enables the W_TX_PAYLOAD_NOACK command
			REG_B1(EN_ACK_PAY)		//Enables Payload with ACK
			REG_B1(EN_DPL)			//Enables Dynamic Payload Length
			REG_BM(Reserved, 5)		//Only "00000" allowed
		);

		//Bank 1 registers
		BUFFER_REGISTER(0x00, 1, BANK1_UNKNOWN_00, 4, CONCAT(0x40,0x4B,0x01,0xE2));	//Must write with 0x404B01E2
		BUFFER_REGISTER(0x01, 1, BANK1_UNKNOWN_01, 4, CONCAT(0xC0,0x4B,0x00,0x00));	//Must write with 0xC04B0000
		BUFFER_REGISTER(0x02, 1, BANK1_UNKNOWN_02, 4, CONCAT(0xD0,0xFC,0x8C,0x02));	//Must write with 0xD0FC8C02
		BUFFER_REGISTER(0x03, 1, BANK1_UNKNOWN_03, 4, CONCAT(0x99,0x00,0x39,0x41));	//Must write with 0x99003941
		BUFFER_REGISTER(0x04, 1, BANK1_UNKNOWN_04, 4, CONCAT(0xD9,0x9E,0x86,0x0B));	//Must write with 0xD99E860B(High Power)For single carrier mode:0xD99E8621
		BUFFER_REGISTER(0x05, 1, BANK1_UNKNOWN_05, 4, CONCAT(0x24,0x06,0x7F,0xA6));	//Must write with 0x24067FA6(Disable RSSI)
		BUFFER_REGISTER(0x08, 1, Chip_ID,          4, CONCAT(0x00,0x00,0x00,0x63));	//BEKEN Chip ID: 0x00000063(RFM70)
		BUFFER_REGISTER(0x0C, 1, BANK1_UNKNOWN_0C, 4, CONCAT(0x00,0x73,0x12,0x00));	//Please initialize with 0x00731200
		BUFFER_REGISTER(0x0D, 1, NEW_FEATURE,      4, CONCAT(0x00,0x80,0xB4,0x36));	//Please initialize with 0x0080B436
		BUFFER_REGISTER(0x0E, 1, RAMP,            11, CONCAT(0xFF,0xFF,0xFE,0xF7,0xCF,0x20,0x81,0x04,0x08,0x20,0x41));				//Ramp curve Please write with 0xFFFFFEF7CF208104082041
	};

	#define COMMAND_VAL_FUNC(__VALUE)\
		static constexpr uint8_t value = __VALUE;

#define COMMAND(__NAME, __VALUE)\
	struct __NAME {COMMAND_VAL_FUNC(__VALUE)};

	struct Cmd{
		//000A AAAA, 1-5 bytes, Read command and status registers. AAAAA = 5 bit Register Map Address
		COMMAND(R_REGISTER, 0x00)
		//001A AAAA, 1-5 bytes, Write command and status registers. AAAAA = 5 bit Register Map Address Executable in power down or standby modes only
		COMMAND(W_REGISTER, 0x20)
		//0110 0001, 1-32 byets, Read RX-payload: 1 to 32 bytes. A read operation always starts at byte 0. Payload is deleted from FIFO after it is read. Used in RX mode.
		COMMAND(R_RX_PAYLOAD, 0x61)
		//1010 0000, 1-32 bytes, Write TX-payload: 1 to 32 bytes. A write operation always starts at byte 0 used in TX payload.
		COMMAND(W_TX_PAYLOAD, 0xA0)
		//1110 0001, 0 bytes, Flush TX FIFO, used in TX mode
		COMMAND(FLUSH_TX, 0xE1)
		//1110 0010, 0 bytes, Flush RX FIFO, used in RX mode Should not be executed during transmission of acknowledge, that is, acknowledge package will not be completed
		COMMAND(FLUSH_RX, 0xE2)
		//1110 0011, 0 bytes, Used for a PTX device
		//Reuse last transmitted payload. Packets are repeatedly
		//retransmitted as long as CE is high.
		//TX payload reuse is active until
		//W_TX_PAYLOAD or FLUSH TX is executed. TX
		//payload reuse must not be activated or deactivated
		//during package transmission
		COMMAND(REUSE_TX_PL , 0xE3)
		//0101 0000, 1 bytes,
		//This write command followed by data 0x73 activates
		//the following features:
		//R_RX_PL_WID
		//W_ACK_PAYLOAD • W_TX_PAYLOAD_NOACK
		//
		//A new ACTIVATE command with the same data
		//deactivates them again. This is executable in power
		//down or stand by modes only
		//
		//The R_RX_PL_WID, W_ACK_PAYLOAD, and
		//W_TX_PAYLOAD_NOACK features registers are
		//initially in a deactivated state; a write has no effect, a
		//read only results in zeros on MISO. To activate these
		//registers, use the ACTIVATE command followed by
		//data 0x73. Then they can be accessed as any other
		//register. Use the same command and data to
		//deactivate the registers again.
		//
		//This write command followed by data 0x53 toggles
		//the register bank, and the current register bank
		//number can be read out from REG7
		COMMAND(ACTIVATE, 0x50)
		//0110 0000, Read RX-payload width for the top R_RX_PAYLOAD in the RX FIFO.
		COMMAND(R_RX_PL_WID, 0x60)
		//1010 1PPP, 1-32 bytes, Used in RX mode.
		//Write Payload to be transmitted together with
		//ACK packet on PIPE PPP. (PPP valid in the range
		//from 000 to 101). Maximum three ACK packet
		//payloads can be pending. Payloads with same PPP are
		//handled using first in - first out principle. Write
		//payload: 1– 32 bytes. A write operation always starts at byte 0.
		COMMAND(W_ACK_PAYLOAD, 0xA8)
		//1011 0000, 1-32 bytes, Used in TX mode. Disables AUTOACK on this specific packet
		COMMAND(W_TX_PAYLOAD_NOACK, 0xB0)
		//1111 1111, 0 bytes, No Operation. Might be used to read the STATUS register
		COMMAND(NOP, 0xFF)
	};
}
#endif
